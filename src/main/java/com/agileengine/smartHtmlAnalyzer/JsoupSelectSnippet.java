package com.agileengine.smartHtmlAnalyzer;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.File;
import java.io.IOException;
import java.util.*;

public class JsoupSelectSnippet {

    private static Logger LOGGER = Logger.getLogger(JsoupSelectSnippet.class);

    private static String CHARSET_NAME = "utf8";

    public static void main(String[] args) {
        if (args.length == 0 || args.length == 1) {
            LOGGER.error("Not found args");
            throw new IllegalArgumentException("One or more arguments don't exist");
        }
        String originalResourcePath = args[0];
        String otherResourcePath = args[1];
        String targetElementId = "make-everything-ok-button";
        String cssQuery = "a";

        Optional<Element> originalElementOpt = findElementById(new File(originalResourcePath), targetElementId);
        Optional<Elements> foundElementsOpt = findElementsByQuery(new File(otherResourcePath), cssQuery);
        selectElementByAttributes(originalElementOpt, foundElementsOpt);
    }

    public static void selectElementByAttributes(Optional<Element> originalElementOpt, Optional<Elements> foundElementsOpt) {
        if (!originalElementOpt.isPresent() || !foundElementsOpt.isPresent()) {
            LOGGER.error("Element not found");
            throw new IllegalArgumentException("An original element or found element don't exist");
        }
        Element originalElement = originalElementOpt.get();
        Attributes foundAttributes = originalElement.attributes();
        double attributesHalf = 2.0;
        int minAttributeMatches = (int) Math.ceil(foundAttributes.size()/attributesHalf);

        Elements elements = foundElementsOpt.get();
        elements.stream()
                .filter(element -> {
                    Attributes attributes = element.attributes();
                    Iterator<Attribute> iterator = attributes.iterator();
                    int count = 0;
                    while (iterator.hasNext()) {
                        Attribute atrr = iterator.next();
                        count = foundAttributes.get(atrr.getKey()).contains(atrr.getValue()) ? ++count : count;
                    }
                    return count >= minAttributeMatches;
                })
                .forEach(JsoupSelectSnippet::printPathToConsole);
    }

    public static Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.of(doc.getElementById(targetElementId));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file " + htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    public static Optional<Elements> findElementsByQuery(File htmlFile, String cssQuery) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.of(doc.select(cssQuery));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file " + htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    private static void printPathToConsole(Element element) {
        List<Element> list = new ArrayList<>(element.parents());
        StringBuilder builder = new StringBuilder();
        for (int i = list.size() - 1; i >= 0; i--) {
            Element parent = list.get(i);
            if (!(parent.tagName().equals("html") || parent.tagName().equals("body"))) {
                builder.append(parent.tagName()).append("[").append(parent.elementSiblingIndex() + 1).append("]").append(" > ");
                continue;
            }
            builder.append(parent.tagName()).append(" > ");
        }
        System.out.println("Element path: "+ builder.append(element.tagName()).append("[").append(element.elementSiblingIndex() + 1).append("]").toString());
        System.out.println("Css selector: " + element.cssSelector());
    }
}
